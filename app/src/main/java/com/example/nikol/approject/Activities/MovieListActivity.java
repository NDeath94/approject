package com.example.nikol.approject.Activities;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.support.v7.widget.SearchView;
import android.widget.TextView;

import com.example.nikol.approject.Utils.SearchMovies;
import com.example.nikol.approject.Models.Movie;
import com.example.nikol.approject.Adapters.MoviesAdapter;
import com.example.nikol.approject.R;

import java.net.URL;
import java.util.ArrayList;

public class MovieListActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {
    //The manipulated views in the activity
    private ProgressBar mLoadingProgress;
    private RecyclerView rvMovies;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_list);
        mLoadingProgress = (ProgressBar) findViewById(R.id.pbLoading);
        rvMovies = (RecyclerView) findViewById(R.id.rv_movies);

        //LayoutManager that
        LinearLayoutManager moviesLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rvMovies.setLayoutManager(moviesLayoutManager);
        try {
            URL movieUrl = SearchMovies.buildURL("Zombie");
            new MoviesQueryTask().execute(movieUrl);

        } catch (Exception e) {
            Log.d("Error", e.getMessage());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.movie_list_menu, menu);
        final MenuItem searchItem = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setOnQueryTextListener(this);
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        try {
            URL movieUrl = SearchMovies.buildURL(query);
            new MoviesQueryTask().execute(movieUrl);
        } catch (Exception e) {
            Log.d("Error", e.getMessage());
        }
        return false;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        return false;
    }

    public class MoviesQueryTask extends AsyncTask<URL, Void, String> {

        @Override
        protected String doInBackground(URL... urls) {
            URL searchURL = urls[0];
            String result = null;
            try {
                result = SearchMovies.getJson(searchURL);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
            }
            return result;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mLoadingProgress.setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(String result) {
            TextView tvError = (TextView) findViewById(R.id.tvError);
            mLoadingProgress.setVisibility(View.INVISIBLE);
            if (result == null) {
                rvMovies.setVisibility(View.INVISIBLE);
                tvError.setVisibility(View.VISIBLE);
            } else {
                rvMovies.setVisibility(View.VISIBLE);
                tvError.setVisibility(View.INVISIBLE);
            }
            ArrayList<Movie> movies = SearchMovies.getMoviesFromJson(result);

            MoviesAdapter adapter = new MoviesAdapter(movies);
            rvMovies.setAdapter(adapter);
        }
    }
}
