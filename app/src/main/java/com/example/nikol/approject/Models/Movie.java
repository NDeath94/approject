package com.example.nikol.approject.Models;

import android.databinding.BindingAdapter;
import android.os.Parcel;
import android.os.Parcelable;
import android.widget.ImageView;

public class Movie implements Parcelable {
    //Fields
    private String title;
    private String releaseDate;
    private String vote_average;
    private String overview;
    private String id;
    private String poster_path;

    //Constructor
    public Movie(String title, String releaseDate, String vote_average, String overview, String id, String poster_path) {
        this.title = title;
        this.releaseDate = releaseDate;
        this.vote_average = vote_average;
        this.overview = overview;
        this.id = id;
        this.poster_path = poster_path;
    }

    protected Movie(Parcel in) {
        title = in.readString();
        releaseDate = in.readString();
        vote_average = in.readString();
        overview = in.readString();
        id = in.readString();
        poster_path = in.readString();
    }

    public static final Creator<Movie> CREATOR = new Creator<Movie>() {
        @Override
        public Movie createFromParcel(Parcel in) {
            return new Movie(in);
        }

        @Override
        public Movie[] newArray(int size) {
            return new Movie[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(title);
        parcel.writeString(releaseDate);
        parcel.writeString(vote_average);
        parcel.writeString(overview);
        parcel.writeString(id);
        parcel.writeString(poster_path);
    }

    //Getters and Setters
    public String getTitle() {
        return title;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public String getVote_average() {
        return vote_average;
    }

    public String getOverview() {
        return overview;
    }

    public String getId() {
        return id;
    }

    public String getPoster_path() {
        return poster_path;
    }
}
