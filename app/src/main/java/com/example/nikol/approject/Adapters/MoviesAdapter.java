package com.example.nikol.approject.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.nikol.approject.Activities.MovieDetailsActivity;
import com.example.nikol.approject.Models.Movie;
import com.example.nikol.approject.R;

import java.util.ArrayList;

/**
 * Created by Nikolai on 23-02-2018.
 * This is the Movie List Activity. It shows a list of movies from a JSON object from the TMDB web api.
 */

public class MoviesAdapter extends RecyclerView.Adapter<MoviesAdapter.MovieViewHolder> {
    private ArrayList<Movie> movies;

    public MoviesAdapter(ArrayList<Movie> movies) {
        this.movies = movies;
    }

    @Override
    public MovieViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        View itemView = LayoutInflater.from(context).inflate(R.layout.movie_list_item, parent, false);
        return new MovieViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MovieViewHolder holder, int position) {
        Movie movie = movies.get(position);
        holder.bind(movie);
    }

    @Override
    public int getItemCount() {
        return movies.size();
    }

    public class MovieViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvTitle;
        TextView tvSubtitle;
        TextView tvSubSubtitle1;
        TextView tvSubSubtitle2;

        public MovieViewHolder(View itemView) {
            super(itemView);
            tvTitle = (TextView) itemView.findViewById(R.id.tvTitle);
            tvSubtitle = (TextView) itemView.findViewById(R.id.tvSubtitle);
            tvSubSubtitle1 = (TextView) itemView.findViewById(R.id.tvSubSubtitle1);
            tvSubSubtitle2 = (TextView) itemView.findViewById(R.id.tvSubSubtitle2);
            itemView.setOnClickListener(this);
        }

        public void bind(Movie movie) {
            tvTitle.setText(movie.getTitle());
            tvSubtitle.setText(movie.getReleaseDate());
            tvSubSubtitle1.setText("ID: " + movie.getId());
            tvSubSubtitle2.setText("Score: " + movie.getVote_average());
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            Movie selectedMovie = movies.get(position);
            Intent intent = new Intent(view.getContext(), MovieDetailsActivity.class);
            intent.putExtra("Movie", selectedMovie);
            view.getContext().startActivity(intent);
        }
    }
}
