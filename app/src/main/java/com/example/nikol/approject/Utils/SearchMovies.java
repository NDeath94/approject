package com.example.nikol.approject.Utils;

import android.net.Uri;
import android.util.Log;

import com.example.nikol.approject.Models.Movie;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Scanner;


/**
 * Created by nikol on 21-02-2018.
 */

public class SearchMovies {
    //Fields for data
    private static final String BASE_API_URL = "https://api.themoviedb.org/3/search/movie";
    private static final String KEY = "api_key";
    private static final String API_KEY = "5efee822a961bc8a4ce567b867dfa166";
    private static final String QUERY_PARAMETER_KEY = "query";

    //http://image.tmdb.org/t/p/w185/ + IMG_QUERY

    //Builds the URL for searching movies on TMDB
    public static URL buildURL(String title) {
        URL url = null;
        Uri uri = Uri.parse(BASE_API_URL).buildUpon().appendQueryParameter(KEY, API_KEY).appendQueryParameter(QUERY_PARAMETER_KEY, title).build();
        try {
            url = new URL(uri.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return url;
    }

    //Gets the data as a JSON object
    public static String getJson(URL url) throws IOException {
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        try {
            InputStream stream = connection.getInputStream();
            Scanner scanner = new Scanner(stream);
            scanner.useDelimiter("\\A");
            boolean hasData = scanner.hasNext();
            if (hasData) {
                return scanner.next();
            } else {
                return null;
            }
        } catch (Exception e) {
            Log.d("Error", e.toString());
            return null;
        } finally {
            connection.disconnect();
        }
    }

    //Returns an array of movies from the JSON object (See the Movie class for what is considered a movie)
    public static ArrayList<Movie> getMoviesFromJson(String json) {
        final String TITLE = "title";
        final String RELEASE_DATE = "release_date";
        final String VOTE_AVERAGE = "vote_average";
        final String OVERVIEW = "overview";
        final String ID = "id";
        final String POSTER_PATH = "poster_path";
        final String RESULTS = "results";
        ArrayList<Movie> movies = new ArrayList<Movie>();
        try {
            JSONObject jsonMovies = new JSONObject(json);
            JSONArray arrayMovies = jsonMovies.getJSONArray(RESULTS);
            int numberOfMovies = arrayMovies.length();
            for (int i = 0; i < numberOfMovies; i++) {
                JSONObject movieJSON = arrayMovies.getJSONObject(i);
                Movie movie = new Movie(
                        movieJSON.getString(TITLE),
                        movieJSON.getString(RELEASE_DATE),
                        movieJSON.getString(VOTE_AVERAGE),
                        movieJSON.getString(OVERVIEW),
                        movieJSON.getString(ID),
                        movieJSON.getString(POSTER_PATH)
                );
                movies.add(movie);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return movies;
    }
}
